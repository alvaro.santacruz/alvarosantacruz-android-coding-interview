package com.beon.androidchallenge.ui.main

import com.beon.androidchallenge.domain.model.Fact

sealed class FactUiState {
    object Empty: FactUiState()
    object Loading: FactUiState()
    object Error: FactUiState()
    data class Success(val fact: Fact): FactUiState()
}