package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact

class MainViewModel : ViewModel() {

    val currentFact = MutableLiveData<FactUiState>(FactUiState.Empty)

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            currentFact.postValue(FactUiState.Empty)
            return
        }
        currentFact.postValue(FactUiState.Loading)

        FactRepository.getInstance()
            .getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
                override fun onResponse(response: Fact) {
                    currentFact.postValue(
                        FactUiState.Success(response)
                    )
                }

                override fun onError() {
                    currentFact.postValue(FactUiState.Error)
                }

            })
    }

    fun saveFavorite() {
        (currentFact.value as? FactUiState.Success)?.let { uiState ->
            val newFact = FactRepository.getInstance().saveAsFavorite(uiState.fact)
            currentFact.value = FactUiState.Success(newFact)
        }
    }
}