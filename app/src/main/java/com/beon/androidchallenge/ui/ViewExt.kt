package com.beon.androidchallenge.ui

import android.text.Editable
import android.view.View
import android.widget.EditText
import androidx.core.os.postDelayed
import androidx.core.widget.doAfterTextChanged


fun EditText.debounce(delay: Long, action: (Editable?) -> Unit) {
    doAfterTextChanged { text ->
        var counter = getTag(id) as? Int ?: 0
        handler.removeCallbacksAndMessages(counter)
        handler.postDelayed(delay, ++counter) { action(text) }
        setTag(id, counter)
    }
}

fun View.setOnDoubleClickListener(listener: (View?) -> Unit) {
    this.setOnClickListener(object : DoubleClickListener() {
        override fun onDoubleClick(v: View?) {
            listener.invoke(v)
        }
    })
}

abstract class DoubleClickListener : View.OnClickListener {
    var lastClickTime: Long = 0
    override fun onClick(v: View?) {
        val clickTime = System.currentTimeMillis()
        if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
            onDoubleClick(v)
        }
        lastClickTime = clickTime
    }

    abstract fun onDoubleClick(v: View?)

    companion object {
        private const val DOUBLE_CLICK_TIME_DELTA: Long = 300 //milliseconds
    }
}