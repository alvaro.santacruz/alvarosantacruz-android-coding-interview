package com.beon.androidchallenge.ui.main

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.core.os.postDelayed
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beon.androidchallenge.R
import com.beon.androidchallenge.databinding.MainFragmentBinding
import com.beon.androidchallenge.ui.debounce
import com.beon.androidchallenge.ui.setOnDoubleClickListener

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.run {
            numberEditText.debounce(400) {
                viewModel.searchNumberFact(it.toString())
            }

            factTextView.setOnDoubleClickListener {
                viewModel.saveFavorite()
            }

            numberEditText.setOnEditorActionListener { textView, actionId, keyEvent ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    viewModel.searchNumberFact(textView.text.toString())
                    return@setOnEditorActionListener true
                } else {
                    return@setOnEditorActionListener false
                }
            }

            retryButton.setOnClickListener {
                viewModel.searchNumberFact(numberEditText.text.toString())
            }

            numberEditText.requestFocus()
        }
    }

    private fun initObservers() {
        viewModel.currentFact.observeForever {
            when(it) {
                is FactUiState.Empty -> {
                    binding.loader.isVisible = false
                    binding.factTextView.isVisible = true
                    binding.imageFavorite.isVisible = false
                    binding.factTextView.setText(R.string.instructions)
                }
                is FactUiState.Loading -> {
                    binding.loader.isVisible = true
                    binding.factTextView.isVisible = false
                    binding.imageFavorite.isVisible = false
                    binding.factTextView.text = ""
                }
                is FactUiState.Error -> {
                    binding.loader.isVisible = false
                    binding.factTextView.isVisible = true
                    binding.imageFavorite.isVisible = false
                    binding.factTextView.setText(R.string.error)
                }
                is FactUiState.Success -> {
                    binding.loader.isVisible = false
                    binding.factTextView.isVisible = true
                    binding.imageFavorite.isVisible = it.fact.favorite ?: false
                    binding.factTextView.text = it.fact.text
                }
            }
        }
    }

}