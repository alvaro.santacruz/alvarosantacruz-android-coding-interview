package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.domain.model.Fact

class FactLocalDataSource {

    companion object {
        // InMemory DB. should use Room instead
        private val dbList = mutableListOf<Fact>()

        private fun findFactLocally(fact: Fact): Fact? {
            return dbList.firstOrNull { it.text == fact.text }
        }
    }

    fun saveFavorite(fact: Fact): Fact {
        if (findFactLocally(fact) == null) {
            dbList.add(fact)
        }
        return fact.also { it.favorite = true }
    }

    fun isFactFavorite(fact: Fact): Boolean {
//        Call Room inside here.
        val found = findFactLocally(fact)
        return found != null
    }

}